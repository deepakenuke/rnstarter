import React from 'react';
import { StatusBar, View } from 'react-native';
import { Provider } from "react-redux";
import { PersistGate } from 'redux-persist/integration/react';
import { persistor, store } from 'src/redux/store';
import MyNavigationContainer from 'src/router/MyNavigationContainer';


const App: Function = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <StatusBar barStyle="light-content" backgroundColor={'red'} />
        <MyNavigationContainer />
      </PersistGate>
    </Provider>
  );
};


export default App
