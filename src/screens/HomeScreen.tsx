import { useNavigation } from "@react-navigation/native"
import React from "react"
import { View, Button, Text } from "react-native"
import { useSelector, shallowEqual } from "react-redux"

const HomeScreen = () => {
    const navigator = useNavigation()

    const { userData } = useSelector((state: any) => ({
        userData: state.userDataReducer || 1,
    }), shallowEqual);

    return (
        <View style={{ flex: 1 }}>
            <Button title={"Go to 2nd Screen"} onPress={() => {
                navigator.navigate("Dashboard")
            }}></Button>
            <Text>{userData}</Text>
        </View>
    )
}


export default HomeScreen