import { useNavigation } from "@react-navigation/native"
import React from "react"
import { View, Button } from "react-native"
import { shallowEqual, useDispatch, useSelector } from "react-redux"
import * as Actions from 'src/redux/actions'


const Dashboard = () => {
    const navigator = useNavigation()
    const dispatch = useDispatch()

    const { userData } = useSelector((state: any) => ({
        userData: state.userDataReducer || 1,
    }), shallowEqual);

    return (
        <View style={{ flex: 1 }}>
            <Button title={"Go to 1st Screen"} onPress={() => {
                dispatch(Actions.userDataAction(userData + 1))
                navigator.goBack()
            }}></Button>
        </View>
    )
}


export default Dashboard