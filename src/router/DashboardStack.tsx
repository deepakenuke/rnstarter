import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "src/screens/HomeScreen";
import React from 'react';
import Dashboard from "src/screens/Dashboard";



const AuthStack = createStackNavigator()

const DashboardStack = () => (
    <AuthStack.Navigator>
        <AuthStack.Screen name="Home" component={HomeScreen} options={{
            title: 'My home',
            headerStyle: {
                backgroundColor: '#f4511e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }} />
        <AuthStack.Screen name="Dashboard" component={Dashboard} options={{
            title: 'Dashboard',
            headerStyle: {
                backgroundColor: '#f4511e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }} />
    </AuthStack.Navigator>
)

export default DashboardStack