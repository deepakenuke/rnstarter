import { NavigationContainer, useNavigation } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import DashboardStack from "./DashboardStack";
import React from 'react';

const MyNavigationContainer = () => {
  return (
    <NavigationContainer >
      <DashboardStack />
    </NavigationContainer>
  );

}


export default MyNavigationContainer

