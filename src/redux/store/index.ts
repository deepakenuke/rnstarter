
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Reducer } from "react";
import { applyMiddleware, combineReducers, createStore, Store } from "redux";
import { Persistor, persistReducer, persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import * as Reducers from 'src/redux/reducers';
import { rootSaga } from 'src/redux/saga/index';


const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
    // Root
    key: 'root',
    // Storage Method (React Native)
    storage: AsyncStorage,
    // Whitelist (Save Specific Reducers)
    whitelist: [
        "userDataReducer",
        "deviceTokenReducer",
        "userTokenReducer",
    ],
    blacklist: [],
    throttle: 1000,
    debounce: 1000,
};

const rootReducer = combineReducers({
    isLoadingReducer: Reducers.isLoadingReducer,
    loadingMsgReducer: Reducers.loadingMsgReducer,
    internetReducer: Reducers.internetReducer,
    userDataReducer: Reducers.userDataReducer,
    deviceTokenReducer: Reducers.deviceTokenReducer,
    userTokenReducer: Reducers.userTokenReducer,
})

const persistedReducer = persistReducer(persistConfig, rootReducer);

let store: Store = createStore(
    persistedReducer,/* preloadedState, */
    applyMiddleware(sagaMiddleware)
)

// Middleware: Redux Persist Persister
let persistor: Persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export { store, persistor };

