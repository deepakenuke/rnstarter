import { store } from 'src/redux/store';
import ActionType from 'src/redux/actions/actionType';

export const setLoadingAction = (item: boolean) => {
  if (item == false) {
    store.dispatch(setLoadingMsg("Loading . . ."))
  }
  return {
    type: ActionType.IS_LOADING,
    payload: item
  }
}

export const setLoadingMsg = (item: string) => {
  return {
    type: ActionType.LOADING_MSG,
    payload: item
  }
}

export const internetConnected = () => {
  console.log('ACTION_CONNECTED')
  return {
    type: ActionType.INTERNET_CONNECTED
  };
}

export const internetDisconnected = () => {
  console.log('ACTION_DISCONNECTED')
  return {
    type: ActionType.INTERNET_DISCONNECTED
  };
}
