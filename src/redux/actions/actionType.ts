const ActionType = {
    IS_LOADING: "IS_LOADING",
    LOADING_MSG: "LOADING_MSG",
    USER_TOKEN: "USER_TOKEN",
    DEVICE_TOKEN_REDUCER: "DEVICE_TOKEN_REDUCER",
    USER_DATA: "USER_DATA",
    INTERNET_CONNECTED: "INTERNET_CONNECTED",
    INTERNET_DISCONNECTED: "INTERNET_DISCONNECTED"
}

export interface action {
    type: String,
    payload: any
}

export default ActionType