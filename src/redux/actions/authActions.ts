import ActionType from 'src/redux/actions/actionType';

export const userDataAction = (userData: object | undefined) => {
    return {
        type: ActionType.USER_DATA,
        payload: userData
    };
}

export const userTokenAction = (userData: string | undefined) => {
    return {
        type: ActionType.USER_DATA,
        payload: userData
    };
}