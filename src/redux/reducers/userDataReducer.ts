import ActionType, { action } from 'src/redux/actions/actionType';


export const userDataReducer = (state: any = null, action: action) => {
    switch (action.type) {
        case ActionType.USER_DATA:
            return action.payload
        default:
            return state
    }
}

export const deviceTokenReducer = (state = null, action: action) => {
    switch (action.type) {
        case ActionType.DEVICE_TOKEN_REDUCER:
            return action.payload
        default:
            return state
    }
}

export const userTokenReducer = (state = null, action: action) => {

    switch (action.type) {
        case ActionType.USER_TOKEN:
            return action.payload
        default:
            return state
    }
}
