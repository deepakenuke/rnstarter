import ActionType, { action } from 'src/redux/actions/actionType';

export const internetReducer = (state = false, action: action) => {
    switch (action.type) {
        case ActionType.INTERNET_CONNECTED:
            return state = true
        case ActionType.INTERNET_DISCONNECTED:
            return state = false
        default:
            return state
    }
}

export const isLoadingReducer = (state = false, action: action) => {
    switch (action.type) {
        case ActionType.IS_LOADING:
            return action.payload
        default:
            return state
    }
}


export const loadingMsgReducer = (state = "Loading . . .", action: action) => {

    switch (action.type) {
        case ActionType.LOADING_MSG:
            return action.payload
        default:
            return state
    }
}




