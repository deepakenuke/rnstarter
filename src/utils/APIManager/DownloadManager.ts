// import { Platform, ToastAndroid } from 'react-native';
// import { check, PERMISSIONS, request, RESULTS } from 'react-native-permissions';
// import RNFetchBlob from 'rn-fetch-blob';
// import { setLoadingAction } from '../../redux/actions';
// import { store } from '../../redux/store';

// export const downloadFile = async (url, fileType, file_label, hideNotification) => {

//     url = url.replace(/%2F/, "/");
//     const permission = Platform.OS == 'ios' ? PERMISSIONS.IOS.PHOTO_LIBRARY : PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE

//     check(permission)
//         .then((result) => {
//             switch (result) {
//                 case RESULTS.UNAVAILABLE:
//                     console.log(
//                         'This feature is not available (on this device / in this context)',
//                     );
//                     break;
//                 case RESULTS.DENIED:
//                     console.log(
//                         'The permission has not been requested / is denied but requestable',
//                     );
//                     request(permission).then((result) => {
//                         if (result == RESULTS.GRANTED) {
//                             download(url, fileType, file_label, hideNotification)
//                         }
//                     });
//                     break;
//                 case RESULTS.GRANTED:
//                     download(url, fileType, file_label, hideNotification)
//                     break;
//                 case RESULTS.BLOCKED:
//                     console.log('The permission is denied and not requestable anymore');
//                     break;
//             }
//         })
//         .catch((error) => {
//             // …
//         });
// }




// export const downloadFileArray = async (fileArray) => {

//     fileArray.forEach((item, index) => {
//         downloadFile(item.file_url, item.file_type, item.file_label, true)
//     });

// }

// const download = async (url, fileType, file_label, hideNotification) => {
//     let dirs = Platform.OS == 'ios' ? RNFetchBlob.fs.dirs.MainBundleDir : RNFetchBlob.fs.dirs.SDCardDir + "/Eschool Downloads/Images/";
//     let extension
//     let name = url.substr(url.lastIndexOf('/') + 1)

//     if (Platform.OS == 'android') {
//         if (fileType && (fileType.includes('jpg') || fileType.includes('jpeg') || name.includes('jpg'))) {
//             extension = '.jpg'
//         }
//         if (fileType && (fileType.includes('png'))) {
//             extension = '.png'
//         }
//         if (name.includes('pdf') || (fileType && (fileType.includes('pdf')))) {
//             extension = '.pdf'
//             dirs = RNFetchBlob.fs.dirs.SDCardDir + "/Eschool Downloads/Documents/"
//         }
//     }
//     console.log(dirs, 'document path');
//     console.log(name, 'document path');
//     console.log(file_label, 'document path');
//     // setTimeout(() => { MyAlert("", "Downloading in progress") }, 200);
//     let exists = false
//     if (Platform.OS == 'android') {
//         await RNFetchBlob.fs.exists((dirs + name))
//             .then((data) => {
//                 console.log("DATA", data)
//                 exists = data
//             }).catch((e) => {
//                 console.log("ERROR", JSON.stringify(e))
//                 if (e)
//                     exists = false

//             })
//     }

//     // return
//     console.log("EXISTS", JSON.stringify(exists))

//     if (exists == false) {
//         store.dispatch(setLoadingAction(true))
//         RNFetchBlob.config({
//             // add this option that makes response data to be stored as a file,
//             // this is much more performant.
//             fileCache: true,
//             path: dirs + name,
//             addAndroidDownloads: {
//                 useDownloadManager: true,
//                 // title: (file_label ? file_label : '') + name,
//                 // path: dirs + (file_label ? file_label : '') + name,
//                 title: name,
//                 path: dirs + name,
//                 notification: true
//             }
//         })
//             .fetch('GET', url, {
//                 //some headers ..

//             }).uploadProgress({ interval: 250 }, (written, total) => {
//                 console.log('uploaded', written / total)
//             })
//             // listen to download progress event
//             .progress({ count: 10 }, (received, total) => {
//                 console.log('progress', received / total)
//             })
//             .then((res) => {
//                 // the temp file path
//                 if (Platform.OS === "ios") {
//                     store.dispatch(setLoadingAction(false))

//                     // RNFetchBlob.ios.openDocument(res.data);
//                 }
//                 if (!hideNotification) ToastAndroid.show("File Saved Successfully", ToastAndroid.CENTER)
//                 console.log('The file saved to ', res.path())
//                 store.dispatch(setLoadingAction(false))

//             }).catch((e) => {
//                 store.dispatch(setLoadingAction(false))
//             }).finally(() => {
//                 store.dispatch(setLoadingAction(false))
//             })
//     } else {
//         if (!hideNotification) ToastAndroid.show("File Already Downloaded", ToastAndroid.CENTER)
//     }
// }