
const baseUrl = "http://cargo.yiipro.com"


const api = "/api/v1/"

export const imageBaseUrl = baseUrl + '/'

export const GEO_API_KEY = "AIzaSyAosp8khPAKxzalNtbz-fBup1VfPXMd3_A"
// export const GEO_API_KEY = "AIzaSyALTt5V3_2zClONWGccm8UrLK626-gi4Vs"


const APIConstants = {
  origin: baseUrl,
  login: baseUrl + api + 'auth/login',
  customerRegistration: baseUrl + api + 'customer/register',
  logout: baseUrl + api + 'auth/logout',
  getCountryList: baseUrl + api + 'countries',
  getStateList: baseUrl + api + 'states',
  getCityList: baseUrl + api + 'cities',
  changePassword: baseUrl + api + 'auth/change-password',
  customer_profile_update: baseUrl + api + 'customer/profile/update',
  equipments: baseUrl + api + 'equipments',
  vehicle_types: baseUrl + api + 'vehicle-types',
  company_vehicle: baseUrl + api + 'company-vehicles/',
  model_vehicle_types: baseUrl + api + 'model-vehicle-types/',
  forgetPassword: baseUrl + api + 'auth/forgot-password',
  resetPassword: baseUrl + api + 'auth/reset-password',
  getProfileDetail: baseUrl + api + 'customer/get',
  getAllCoupons: baseUrl + api + 'booking/coupons',
  calculatePrice: baseUrl + api + 'booking/calculate-price',
  bookOrder: baseUrl + api + 'booking/create',
  bookingHistory: baseUrl + api + 'customer/booking-history',
  bookingHistoryDetail: baseUrl + api + 'booking/',
  cancelBooking: baseUrl + api + 'customer/cancel-booking',
  getAllAddresses: baseUrl + api + 'customer/addresses',
  deleteAddress: baseUrl + api + 'customer/address/delete/',
  addAddress: baseUrl + api + 'customer/address/add',
  getAddressById: baseUrl + api + 'customer/address/get/',
  updateAddressById: baseUrl + api + 'customer/address/update/',
  getBookingIdForDispute: baseUrl + api + 'dispute/booking-id-list',
  createDispute: baseUrl + api + 'dispute/create',
  createQuery: baseUrl + api + 'query/create',
  rateBooking: baseUrl + api + 'booking/rating',
  callPayment: "https://us-central1-fourexpresscargo.cloudfunctions.net/payOnStripe",
  generateOTP: baseUrl + api + '/auth/genrate-otp',
  verifyOTP: baseUrl + api + '/auth/verify-otp',
  getProducts: baseUrl + api + 'customer/product/get-all',
  getVehicleTypesByLocation: baseUrl + api + 'customer/vehicle/by-lat-long',
  addProduct: baseUrl + api + 'customer/product/create',
}


export default APIConstants;

