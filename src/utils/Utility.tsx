// import moment from "moment";
// import 'moment-precise-range-plugin';
import { Alert, Linking, Platform } from "react-native";
// import { openSettings, PERMISSIONS, request, RESULTS } from 'react-native-permissions';
import StringConstants from "./StringConstants";

export function roundOffTwo(num: number) {
    return Math.round(num * 100) / 100
}

export function isEmailValid(text: string) {
    let reg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!reg.test(text)) {
        return false
    } else {
        return true
    }
}


// export function getAge(date1, date2) {
//     try {
//         let age = moment.preciseDiff(date2 ? date2 : new Date(), date1, true);
//         let newAge = (age.years + (age.months / 12) + (age.days / 365))
//         //console.log(newAge)
//         return newAge
//     }
//     catch (e) {
//         console.log(e)
//         // alert(e)
//         return 0
//     }
// }

export function MyAlert(title: string, msg: string, okHandler = () => null, cancel = false) {
    if (cancel)
        return (
            Alert.alert(
                title,
                '' + msg + '',
                [
                    { text: 'Ok', onPress: okHandler },
                    { text: 'Cancel' }
                ]
            )
        )
    return (
        Alert.alert(
            title,
            '' + msg + '',
            [
                { text: 'Ok', onPress: okHandler }
            ], { cancelable: false }
        )
    )
}

// export function hmsToSecondsOnly(str) {
//     if (str) {
//         let ss = moment.duration(str).asSeconds()
//         return ss
//     }
// }

// export function secondsToHMS(secs) {
//     if (secs) {
//         let p = moment.utc(secs * 1000).format('HH:mm:ss').split(':')
//         return (p[0] + 'h : ' + p[1] + 'm : ' + p[2] + 's');
//     }
// }

// export function getStartingLetters(s) {
//     const res = s.split(" ");
//     let name = ''
//     try {
//         name = res[0][0].toUpperCase();
//         if (res.length > 1) {
//             name = name + res[res.length - 1][0].toUpperCase();
//         }
//     }
//     catch (e) {

//     }
//     return name
// }


export function openLink(link: string) {
    if (!link) return
    if (!link.includes("http")) {
        link = "http://" + (link.trim())
    }
    Linking.openURL(link).catch((err: any) => {
        console.error("Couldn't load page", err)
        alert(err)
    });
}

/**
 *
 * @param {* input input key} input
 * @param {* type of input key (key|value,code,shortName)} inputType
 * @param {* return Type of Key (key,value,code,shortName)} returnType
 */
// export function getStatus(input: any, inputType: string, returnType: string) {
//     try {
//         // console.log("input",JSON.stringify(input))

//         const statuses = StringConstants.STATUS
//         const status = statuses.filter((item: { [x: string]: any; }, index: any) => {
//             return JSON.stringify(item[inputType]) == JSON.stringify(input)
//         })
//         // console.log("getStatus",JSON.stringify(status[0][returnType]))
//         return status[0][returnType]
//     }
//     catch (e) {
//         return ""
//     }
// }

// export function callOnNumber(number: any) {
//     if (Platform.OS == 'ios') {
//         RNImmediatePhoneCall.immediatePhoneCall(number)
//     } else {
//         request(PERMISSIONS.ANDROID.CALL_PHONE).then((result: any) => {
//             switch (result) {
//                 case RESULTS.UNAVAILABLE:
//                     console.log(
//                         'This feature is not available (on this device / in this context)',
//                     );
//                     alert('This feature is not available on this device')
//                     break;
//                 case RESULTS.DENIED:
//                     console.log(
//                         'The permission has not been requested / is denied but requestable',
//                     );

//                     break;
//                 case RESULTS.GRANTED:
//                     console.log('The permission is granted');
//                     RNImmediatePhoneCall.immediatePhoneCall(number)
//                     break;
//                 case RESULTS.BLOCKED:
//                     console.log('The permission is denied and not requestable anymore');
//                     showSettingsAlert();
//                     // openSettings().catch(() => console.warn('cannot open settings'))
//                     break;
//             }
//         })
//             .catch((error: any) => {

//             });
//     }
// }

// export function showSettingsAlert() {
//     Alert.alert("Permission", "To make phone call, allow App access to your phone call. Tap Settings > Permissions, and turn Phone on?",
//         [
//             {
//                 text: "OK",
//                 onPress: () => { openSettings().catch(() => console.warn('cannot open settings')) }
//             },
//             {
//                 text: "Cancel",
//                 onPress: () => { }
//             },
//         ])
// }

// export function statusEquals(status: any, array: any[]) {
//     let returnValue = false
//     array.some((el: any, index: any) => {
//         if (status == getStatus(el, "id", "status")) {
//             returnValue = true
//             return true
//         }
//     })
//     return returnValue
// }

